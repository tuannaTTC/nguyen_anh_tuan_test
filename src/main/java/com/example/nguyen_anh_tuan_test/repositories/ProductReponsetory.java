package com.example.nguyen_anh_tuan_test.repositories;

import com.example.nguyen_anh_tuan_test.models.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductReponsetory extends JpaRepository<Product, Integer> {

    @Query(nativeQuery = true, value = "SELECT products.name as name,products.category_tag as category_tag,category_tags.name as category_name,products.price as price\n" +
            "                           FROM products\n" +
            "                           INNER JOIN category_tags\n" +
            "                           ON category_tags.tag = products.category_tag\n" +
            "                           WHERE category_tags.name like %:name% " +
            "                           ORDER BY price ASC ")
    List<List<?>> filterByCategoryName(@Param("name") String categoryName);
}
