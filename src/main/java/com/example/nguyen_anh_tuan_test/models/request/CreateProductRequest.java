package com.example.nguyen_anh_tuan_test.models.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class CreateProductRequest {
    @NotBlank
    private String name;

    @NotBlank
    private String categoryTag;

    @Min(0)
    private Double price;

    public CreateProductRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryTag() {
        return categoryTag;
    }

    public void setCategoryTag(String categoryTag) {
        this.categoryTag = categoryTag;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
