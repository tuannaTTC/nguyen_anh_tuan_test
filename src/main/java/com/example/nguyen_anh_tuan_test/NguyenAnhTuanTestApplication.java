package com.example.nguyen_anh_tuan_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NguyenAnhTuanTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(NguyenAnhTuanTestApplication.class, args);
    }

}
